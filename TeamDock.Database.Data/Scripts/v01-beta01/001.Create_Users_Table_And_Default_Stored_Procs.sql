﻿USE [TeamDock.Data]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Insert initial configuration values
USE [TeamDock.Data]
GO

INSERT INTO [dbo].[Configuration]
           ([Key]
           ,[Value])
     VALUES
           ('account_activation_token_expire_time_hours'
           ,'168')
GO

INSERT INTO [dbo].[Configuration]
           ([Key]
           ,[Value])
     VALUES
           ('password_reset_token_expire_time_hours'
           ,'24')
GO




-- Create the Users table
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Password] [nvarchar](256) NULL,
	[ActivationToken] [uniqueidentifier] NULL,
	[ActivationTokenExpireTime] [datetime] NULL,
	[ActivationTime] [datetime] NULL,
	[PasswordHistory] [nvarchar](MAX) NULL,
	[PasswordResetToken] [uniqueidentifier] NULL,
	[PasswordResetTokenExpireTime] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 
GO




-- Create stored procedure for fetching a users using the Email
IF OBJECT_ID('[dbo].[SP_USER_GET_BY_EMAIL]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_GET_BY_EMAIL]
GO

CREATE PROCEDURE [dbo].[SP_USER_GET_BY_EMAIL]
	@in_Email nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;

   SELECT [Id], [Email], [FirstName], [LastName]
   FROM [dbo].[Users]
   WHERE [Email] = @in_Email;
END
GO




-- Create stored procedure for fetching a users using the unique identifier
IF OBJECT_ID('[dbo].[SP_USER_GET]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_GET]
GO

CREATE PROCEDURE [dbo].[SP_USER_GET]
	@in_Id uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

   SELECT [Id], [Email], [FirstName], [LastName]
   FROM [dbo].[Users]
   WHERE [Id] = @in_Id;
END
GO




-- Create stored procedure for creating a new user
IF OBJECT_ID('[dbo].[SP_USER_ADD]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_ADD]
GO

CREATE PROCEDURE [dbo].[SP_USER_ADD]
	@in_Id uniqueidentifier,
	@in_Email nvarchar(100),
	@in_FirstName nvarchar(50),
	@in_LastName nvarchar(50),
	@in_Password nvarchar(50)
AS

DECLARE
@var_PasswordHash nvarchar(256);

BEGIN
	SET NOCOUNT ON;

	-- Create password hashes
	SET @var_PasswordHash = HASHBYTES('SHA2_256', @in_Password);

	-- Insert the new user's data
   INSERT INTO [dbo].[Users]
           ([Id]
           ,[Email]
           ,[FirstName]
           ,[LastName]
           ,[Password]
           ,[PasswordHistory])
     VALUES
           (@in_Id
           ,@in_Email
           ,@in_FirstName
           ,@in_LastName
           ,@var_PasswordHash
           ,@var_PasswordHash);
END
GO




-- Create stored procedure for resetting the activation token of an user
IF OBJECT_ID('[dbo].[SP_USER_ACTIVATION_TOKEN_REFRESH]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_ACTIVATION_TOKEN_REFRESH]
GO

CREATE PROCEDURE [dbo].[SP_USER_ACTIVATION_TOKEN_REFRESH]
	@in_UserId uniqueidentifier
AS

DECLARE
@var_UserId uniqueidentifier,
@var_ActivationToken uniqueidentifier,
@var_ActivationTime datetime,
@var_ActivationTokenExpireTimeHours int

BEGIN
	SET NOCOUNT ON;

	-- Extract the necessary data for performing validations
	SELECT @var_UserId = [Id],
		   @var_ActivationTime = [ActivationTime]
	FROM [dbo].[Users]
	WHERE [Id] = @in_UserId;

	-- Verify that the user exists
	IF @var_UserId IS NULL
	BEGIN
		THROW 50000, 'INVALID_USER_ID', 0;
	END;

	-- Verify that the user is not already activated
	IF @var_ActivationTime IS NOT NULL
	BEGIN
		THROW 50000, 'USER_ALREADY_ACTIVATED', 1;
	END;

	-- Generate a new activation token
	SET @var_ActivationToken = NEWID();

	-- Get the configuration value for the token expiration time
	SELECT @var_ActivationTokenExpireTimeHours = CAST([Value] AS INT)
	FROM [dbo].[Configuration]
	WHERE [Key] = 'account_activation_token_expire_time_hours';

	-- Update the activation token for the user
	UPDATE [dbo].[Users]
	SET [ActivationToken] = @var_ActivationToken,
		[ActivationTokenExpireTime] = DATEADD(HOUR, @var_ActivationTokenExpireTimeHours, SYSUTCDATETIME())
	WHERE [Id] = @var_UserId;

	-- Return the new token
	SELECT @var_ActivationToken AS 'ActivationToken';
END
GO




-- Create stored procedure for activating a user
IF OBJECT_ID('[dbo].[SP_USER_ACTIVATE]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_ACTIVATE]
GO

CREATE PROCEDURE [dbo].[SP_USER_ACTIVATE]
	@in_ActivationToken uniqueidentifier
AS

DECLARE
@var_UserId uniqueidentifier,
@var_ActivationTokenExpireTime datetime,
@var_ActivationTime datetime

BEGIN
	SET NOCOUNT ON;

	-- Extract the necessary data for performing validations
	SELECT @var_UserId = [Id],
		   @var_ActivationTokenExpireTime = [ActivationTokenExpireTime],
		   @var_ActivationTime = [ActivationTime]
	FROM [dbo].[Users]
	WHERE [ActivationToken] = @in_ActivationToken;

	-- Verify that an user with that token exists
	IF @var_UserId IS NULL
	BEGIN
		THROW 50000, 'INVALID_ACTIVATION_TOKEN', 0;
	END;

	-- Verify that the token has not expired
	IF SYSUTCDATETIME() > @var_ActivationTokenExpireTime
	BEGIN
		THROW 50000, 'ACTIVATION_TOKEN_EXPIRED', 1;
	END;

	-- Verify that the user is not already activated
	IF @var_ActivationTime IS NOT NULL
	BEGIN
		THROW 50000, 'USER_ALREADY_ACTIVATED', 2;
	END;

	-- Activate the user
	UPDATE [dbo].[Users]
	SET [ActivationTime] = SYSUTCDATETIME(),
		[ActivationToken] = NULL
	WHERE [Id] = @var_UserId;
END
GO




-- Create stored procedure for verifying a user's credentials
IF OBJECT_ID('[dbo].[SP_USER_CREDENTIALS_VALIDATE]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_CREDENTIALS_VALIDATE]
GO

CREATE PROCEDURE [dbo].[SP_USER_CREDENTIALS_VALIDATE]
	@in_Email nvarchar(100), 
	@in_Password nvarchar(50)
AS

DECLARE 
@var_PasswordHash nvarchar(256),
@var_UserId uniqueidentifier

BEGIN
	SET NOCOUNT ON;

	SET @var_PasswordHash = HASHBYTES('SHA2_256', @in_Password);

	SELECT @var_UserId = [Id]
	FROM [dbo].[Users] 
	WHERE [Email] = @in_Email 
	  AND [Password] = @var_PasswordHash;

	SELECT IIF(@var_UserId IS NULL, 0, 1);
END
GO




-- Create stored procedure for changing a user's password
IF OBJECT_ID('[dbo].[SP_USER_PASSWORD_CHANGE]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_PASSWORD_CHANGE]
GO

CREATE PROCEDURE [dbo].[SP_USER_PASSWORD_CHANGE]
	@in_Email nvarchar(100),
	@in_OldPassword nvarchar(50), 
	@in_NewPassword nvarchar(50)
AS

DECLARE 
@var_PasswordHash nvarchar(256),
@var_UserId uniqueidentifier

BEGIN
	SET NOCOUNT ON;

	SET @var_PasswordHash = HASHBYTES('SHA2_256', @in_OldPassword);

	SELECT @var_UserId = [Id]
	FROM [dbo].[Users] 
	WHERE [Email] = @in_Email 
	  AND [Password] = @var_PasswordHash;

	IF @var_UserId IS NULL
	BEGIN
		THROW 50000, 'INVALID_CREDENTIALS', 0;
	END;

	SET @var_PasswordHash = HASHBYTES('SHA2_256', @in_NewPassword);

	UPDATE [dbo].[Users]
	SET [Password] = @var_PasswordHash
	WHERE [Id] = @var_UserId;
END
GO




-- Create stored procedure for resetting a user's password
IF OBJECT_ID('[dbo].[SP_USER_PASSWORD_RESET]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_PASSWORD_RESET]
GO

CREATE PROCEDURE [dbo].[SP_USER_PASSWORD_RESET]
	@in_ResetKey uniqueidentifier,
	@in_NewPassword nvarchar(50)
AS

DECLARE 
@var_PasswordHash nvarchar(256),
@var_UserId uniqueidentifier,
@var_ResetTokenExpireTime datetime

BEGIN
	SET NOCOUNT ON;

	SELECT @var_UserId = [Id], 
		   @var_ResetTokenExpireTime = [PasswordResetTokenExpireTime]
	FROM [dbo].[Users] 
	WHERE [PasswordResetToken] = @in_ResetKey;

	IF @var_UserId IS NULL
	BEGIN
		THROW 50000, 'RESET_KEY_NOT_FOUND', 0;
	END;

	IF SYSUTCDATETIME() > @var_ResetTokenExpireTime
	BEGIN
		THROW 50000, 'RESET_KEY_EXPIRED', 1;
	END;

	SET @var_PasswordHash = HASHBYTES('SHA2_256', @in_NewPassword);

	UPDATE [dbo].[Users]
	SET [Password] = @var_PasswordHash,
		[PasswordResetToken] = NULL
	WHERE [Id] = @var_UserId;
END
GO




-- Create stored procedure for refreshing a user's reset password token
IF OBJECT_ID('[dbo].[SP_USER_PASSWORD_RESET_TOKEN_REFRESH]','P') IS NOT NULL
   DROP PROCEDURE [dbo].[SP_USER_PASSWORD_RESET_TOKEN_REFRESH]
GO

CREATE PROCEDURE [dbo].[SP_USER_PASSWORD_RESET_TOKEN_REFRESH]
	@in_Email nvarchar(100)
AS

DECLARE 
@var_UserId uniqueidentifier,
@var_PasswordResetToken uniqueidentifier,
@var_PasswordResetTokenExpireTimeHours int

BEGIN
	SET NOCOUNT ON;

	SELECT @var_UserId = [Id]
	FROM [dbo].[Users] 
	WHERE [Email] = @in_Email;

	IF @var_UserId IS NULL
	BEGIN
		THROW 50000, 'USER_NOT_FOUND', 0;
	END;

	-- Get the configuration value for the token expiration time
	SELECT @var_PasswordResetTokenExpireTimeHours = CAST([Value] AS INT)
	FROM [dbo].[Configuration]
	WHERE [key] = 'password_reset_token_expire_time_hours';

	SET @var_PasswordResetToken = NEWID();

	UPDATE [dbo].[Users]
	SET [PasswordResetToken] = @var_PasswordResetToken,
		[PasswordResetTokenExpireTime] = DATEADD(HOUR, @var_PasswordResetTokenExpireTimeHours, SYSUTCDATETIME())
	WHERE [Id] = @var_UserId;

	SELECT @var_PasswordResetToken AS 'PasswordResetToken';
END
GO