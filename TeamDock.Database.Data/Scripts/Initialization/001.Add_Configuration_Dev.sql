﻿USE [TeamDock.Data]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

INSERT INTO [dbo].[Configuration]
           ([Key], [Value])
     VALUES
           ('environment', 'dev')
GO