﻿/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2017 (14.0.900)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TeamDock.Data]
GO

/****** Object:  Table [dbo].[Clients]    Script Date: 8/15/2017 11:12:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Create configuration table
DROP TABLE IF EXISTS [dbo].[Configuration]

CREATE TABLE [dbo].[Configuration](
	[Key] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-- Create clients table
DROP TABLE IF EXISTS [dbo].[Clients]

CREATE TABLE [dbo].[Clients](
	[Id] [uniqueidentifier] NOT NULL,
	[ClientIdentity] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Secret] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UNIQUE_Clients_ClientIdentity] UNIQUE NONCLUSTERED 
(
	[ClientIdentity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UNIQUE_Clients_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UNIQUE_Clients_Secret] UNIQUE NONCLUSTERED 
(
	[Secret] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Insert the initial Clients
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES(NEWID()
           ,'teamdock_idsvr'
           ,'TeamDock Identity Server'
           ,'h48qZLhdpHzLCcugWztvuGaCGkpM9sm3tKZxhBn8kFwEXUZ6yE5QpW5XCK7ZK7eDrjxEzuWPtmHRLsxeWFdPvF4uYmZ2MucrXdjwR6PBgXsnd89uvucwz98JCHysGuCs'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES(NEWID()
           ,'teamdock_projectsapi'
           ,'TeamDock Projects API'
           ,'rWFLCxwHpZgCskpvJjyAnHaUYFa2E6gxsEhwgdvxgBF64HNgJsPv9Y8GTarJXETwzRFrCEgGcwvsPHqfwszrDb6phJfEL6ZzwDfCYuRxskkpKU8mcskuyQGRPyVK66Sp'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES(NEWID()
           ,'teamdock_userapi'
           ,'TeamDock User API'
           ,'XLPjMjGHtzNEYrAFxNfyYwuhG46rLwywTmn7LNA3kM6N4anm9xPwkWtZUrPQ7Kz5mEczTXayyCfWzQGcC5EKgbbuKJZz4P9G7bzrsaDxQDKHRM6BrFrZQUnaKKb9zpAq'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES(NEWID()
           ,'teamdock_talkapi'
           ,'TeamDock Talk API'
           ,'uThhKBVCssJKNTXS3T5bYZXexyVVwn7SmYFy6HzS5LDtxMyjf6VTeeyQDHxkDaG5FUg3KWsWH33HEcNk4Qhz9yZpXBZfRqwsjAk9DPECc86wthT2VpGVVXMQb34dvuyv'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES(NEWID()
           ,'teamdock_electron'
           ,'TeamDock Electron Clients'
           ,'jj63JEVYpsT9mnrdGk22YUR2ttpwvkTBDPREeRVjR8sw3c9mDZz9dzxwrhbAk6Gd26DKmJGjRqpLBpNRt82XjFUPJRD46e5FQcaHJEQxs4ecXkeUCAYQb6P28MmjcSBN'
	)
GO
