﻿/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2017 (14.0.900)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TeamDockDb]
GO

/****** Object:  Table [dbo].[Clients]    Script Date: 8/15/2017 11:12:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [dbo].[Configuration]

CREATE TABLE [dbo].[Configuration](
	[Environment] [nvarchar](50) NULL
) ON [PRIMARY]

DROP TABLE IF EXISTS [dbo].[Clients]

CREATE TABLE [dbo].[Clients](
	[Id] [uniqueidentifier] NOT NULL,
	[ClientIdentity] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Secret] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UNIQUE_Clients_ClientIdentity] UNIQUE NONCLUSTERED 
(
	[ClientIdentity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UNIQUE_Clients_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UNIQUE_Clients_Secret] UNIQUE NONCLUSTERED 
(
	[Secret] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Insert the initial Clients
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES('407B3DF6-34E1-4855-A683-28D4F352196D'
           ,'teamdock_idsvr'
           ,'TeamDock Identity Server'
           ,'h48qZLhdpHzLCcugWztvuGaCGkpM9sm3tKZxhBn8kFwEXUZ6yE5QpW5XCK7ZK7eDrjxEzuWPtmHRLsxeWFdPvF4uYmZ2MucrXdjwR6PBgXsnd89uvucwz98JCHysGuCs'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES('36DEDC31-0445-4C58-A695-41B85948E206'
           ,'teamdock_projectsapi'
           ,'TeamDock Projects API'
           ,'rWFLCxwHpZgCskpvJjyAnHaUYFa2E6gxsEhwgdvxgBF64HNgJsPv9Y8GTarJXETwzRFrCEgGcwvsPHqfwszrDb6phJfEL6ZzwDfCYuRxskkpKU8mcskuyQGRPyVK66Sp'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES('BC1562C2-2D7F-4A3F-9F0B-4BFBDF54CF58'
           ,'teamdock_userapi'
           ,'TeamDock User API'
           ,'XLPjMjGHtzNEYrAFxNfyYwuhG46rLwywTmn7LNA3kM6N4anm9xPwkWtZUrPQ7Kz5mEczTXayyCfWzQGcC5EKgbbuKJZz4P9G7bzrsaDxQDKHRM6BrFrZQUnaKKb9zpAq'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES('A3DD8E90-1C3B-4FD1-AC5D-A17046673A3B'
           ,'teamdock_talkapi'
           ,'TeamDock Talk API'
           ,'uThhKBVCssJKNTXS3T5bYZXexyVVwn7SmYFy6HzS5LDtxMyjf6VTeeyQDHxkDaG5FUg3KWsWH33HEcNk4Qhz9yZpXBZfRqwsjAk9DPECc86wthT2VpGVVXMQb34dvuyv'
	)
INSERT INTO [dbo].[Clients]
           ([Id]
           ,[ClientIdentity]
           ,[Name]
           ,[Secret])
     VALUES('E039B543-D4CA-4439-BFA6-D885D38E76E9'
           ,'teamdock_electron'
           ,'TeamDock Electron Clients'
           ,'jj63JEVYpsT9mnrdGk22YUR2ttpwvkTBDPREeRVjR8sw3c9mDZz9dzxwrhbAk6Gd26DKmJGjRqpLBpNRt82XjFUPJRD46e5FQcaHJEQxs4ecXkeUCAYQb6P28MmjcSBN'
	)
GO
