﻿/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2017 (14.0.900)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TeamDockDb]
GO

/****** Object:  Table [dbo].[Configuration]    Script Date: 8/15/2017 11:01:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

INSERT INTO [dbo].[Configuration]
           ([Environment])
     VALUES
           ('development')
GO