﻿using System;
using System.Reflection;
using DbUp;
using DbUp.Engine;
using Microsoft.Extensions.Configuration;

namespace TeamDock.Database.Audit
{
    public class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables("teamdock_")
                .Build();

            var connectionString = configuration["database_connection_string"];
            var environment = configuration["dbup_environment"];

            EnsureDatabase.For.SqlDatabase(connectionString);

            var upgrader = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScript(GetConfigScript(environment))
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .LogToConsole()
                .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success...");
            }

            Console.ResetColor();

#if DEBUG
            Console.ReadKey();
#endif
        }

        private static SqlScript GetConfigScript(string environment)
        {
            switch (environment)
            {
                case "development":
                    return SqlScript.FromFile("Scripts/Initialization/001.Add_Configuration_Dev.sql");
                case "production":
                    return SqlScript.FromFile("Scripts/Initialization/001.Add_Configuration_Prod.sql");
                default:
                    throw new Exception("Bad environment config");
            }
        }
    }
}
